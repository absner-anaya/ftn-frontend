import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IContext } from '../../models/context.model';

@Injectable()
export class LogoutAuthService extends StorageHelper {


  constructor(private http: Http) {  super(); }



  /**
   * prueba
   */
  public logout(): Observable<any> {
    const ctx   =   this.getContext();

    const service_url = ApiHelper.Instance.api_base_url + 'logout';
    // const service_url = 'http://147.135.228.122:8080/Auth/login?email=' + this.email + '&password=' + this.password;


    const headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + ctx.token
    });

    const options = new RequestOptions( { headers: headers } );



    return this.http.get(service_url, options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

}
