import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { ApiHelper } from '../../helpers/api.helper';
import { IContext } from '../../models/context.model';

@Injectable()
export class ChangePasswordAuthService {

  public email: string;
  public password: string;
  public token: string;

  constructor(private http: Http) { }

  /**
   * init
   */
  public init(email_: string, password_: string, token_: string) {
    this.email      =   email_;
    this.password   =   password_;
    this.token      =   token_;
    return this;
  }


  /**
   * prueba
   */
  public changePass(): Observable<any> {

    const service_url = ApiHelper.Instance.api_base_url + 'changePassword';
    // const service_url = 'http://147.135.228.122:8080/Auth/login?email=' + this.email + '&password=' + this.password;

    const params = {
      email : this.email,
      password : this.password,
      token : this.token
    };

    const headers = new Headers({
        'Content-Type': 'application/json'
    });

    const options = new RequestOptions( { headers: headers } );



    return this.http.post(service_url, params, options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

}
