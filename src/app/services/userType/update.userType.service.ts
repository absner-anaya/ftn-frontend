import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IUserType } from '../../models/userType.model';

@Injectable()
export class UpdateUserTypeService extends StorageHelper {

  constructor( private http: Http ) { super(); }

  private userType_id: string;
  private userType_data: IUserType;

  public init( userType_id_: string, userType_data_: IUserType ): UpdateUserTypeService {
    this.userType_id = userType_id_;
    this.userType_data = userType_data_;
    return this;
  }


  public updateUser(): Observable <IUserType> {

    const service_url = ApiHelper.Instance.api_base_url + `user/${this.userType_id}`;
    const ctx   =   this.getContext();

    const _headers = new Headers ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + ctx.token
    });

    const _options = new RequestOptions( { headers: _headers } );

    const _params = this.userType_data;

    return this.http.put( service_url, _params, _options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));

  }

}
