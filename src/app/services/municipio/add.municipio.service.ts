import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IMunicipio } from '../../models/minicipio.model';

@Injectable()

export class AddMunicipioService extends StorageHelper {

    private new_municipio: IMunicipio;

    constructor( private http: Http ) { super(); }

    public init( new_municipio_: IMunicipio ): AddMunicipioService {

        this.new_municipio = new_municipio_;
        return this;
    }


    public createMunicipio(): Observable <IMunicipio> {


        const service_url = ApiHelper.Instance.api_base_url + 'user';
        const ctx   =   this.getContext();


        const _headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + ctx.token
        });

        const _options = new RequestOptions( { headers: _headers } );

        const _params = this.new_municipio;

        return this.http.post( service_url, _params, _options)
            .map((res: Response) => res.json() )
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));

    }

}
