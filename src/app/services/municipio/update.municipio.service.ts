import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IMunicipio } from '../../models/minicipio.model';

@Injectable()
export class UpdateMunicipioService extends StorageHelper {

  constructor( private http: Http ) { super(); }

  private municipio_id: string;
  private municipio_data: IMunicipio;

  public init( municipio_id_: string, municipio_data_: IMunicipio ): UpdateMunicipioService {
    this.municipio_id = municipio_id_;
    this.municipio_data = municipio_data_;
    return this;
  }


  public updateMunicipio(): Observable <IMunicipio> {

    const service_url = ApiHelper.Instance.api_base_url + `user/${this.municipio_id}`;
    const ctx   =   this.getContext();

    const _headers = new Headers ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + ctx.token
    });

    const _options = new RequestOptions( { headers: _headers } );

    const _params = this.municipio_data;

    return this.http.put( service_url, _params, _options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));

  }

}
