import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IPedidoseguimiento } from '../../models/pedidoSeguimiento.model';

@Injectable()
export class UpdatePedidoSeguimientoService extends StorageHelper {

  constructor( private http: Http ) { super(); }

  private pedidoseguimineto_id: string;
  private pedidoseguimineto_data: IPedidoseguimiento;

  public init( pedidoseguimineto_id_: string, pedidoseguimineto_data_: IPedidoseguimiento ): UpdatePedidoSeguimientoService {
    this.pedidoseguimineto_id = pedidoseguimineto_id_;
    this.pedidoseguimineto_data = pedidoseguimineto_data_;
    return this;
  }


  public updatePedido(): Observable <IPedidoseguimiento> {

    const service_url = ApiHelper.Instance.api_base_url + `user/${this.pedidoseguimineto_id}`;
    const ctx   =   this.getContext();

    const _headers = new Headers ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + ctx.token
    });

    const _options = new RequestOptions( { headers: _headers } );

    const _params = this.pedidoseguimineto_data;

    return this.http.put( service_url, _params, _options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));

  }

}
