import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IPedidoseguimiento } from '../../models/pedidoSeguimiento.model';


@Injectable()
export class GetPedidoSeguimientoService extends StorageHelper {
    private pedidoseguimiento_id: string;

    constructor(private http: Http) { super(); }

    public init(pedidoseguimiento_id_: string): GetPedidoSeguimientoService {
        this.pedidoseguimiento_id = pedidoseguimiento_id_;
        return this;
    }

    public getData(): Observable<IPedidoseguimiento> {

        const service_url = ApiHelper.Instance.api_base_url + `user/` + this.pedidoseguimiento_id;
        const ctx   =   this.getContext();

        const _headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + ctx.token
        });

        const _options = new RequestOptions({ headers: _headers });

        return this.http.get(service_url, _options)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));

    }
}
