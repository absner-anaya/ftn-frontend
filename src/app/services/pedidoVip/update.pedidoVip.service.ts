import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IPedidovip } from '../../models/pedidoVip.model';

@Injectable()
export class UpdatePedidoVipService extends StorageHelper {

  constructor( private http: Http ) { super(); }

  private pedidoVip_id: string;
  private pedidoVip_data: IPedidovip;

  public init( pedidoVip_id_: string, pedidoVip_data_: IPedidovip ): UpdatePedidoVipService {
    this.pedidoVip_id = pedidoVip_id_;
    this.pedidoVip_data = pedidoVip_data_;
    return this;
  }


  public updatePedido(): Observable <IPedidovip> {

    const service_url = ApiHelper.Instance.api_base_url + `user/${this.pedidoVip_id}`;
    const ctx   =   this.getContext();

    const _headers = new Headers ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + ctx.token
    });

    const _options = new RequestOptions( { headers: _headers } );

    const _params = this.pedidoVip_data;

    return this.http.put( service_url, _params, _options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));

  }

}
