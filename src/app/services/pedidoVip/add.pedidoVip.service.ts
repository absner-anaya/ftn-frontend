import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IPedidovip } from '../../models/pedidoVip.model';

@Injectable()

export class AddPedidoVipService extends StorageHelper {

    private new_pedidovip: IPedidovip;

    constructor( private http: Http ) { super(); }

    public init( new_pedidovip_: IPedidovip ): AddPedidoVipService {

        this.new_pedidovip = new_pedidovip_;
        return this;
    }


    public createPedido(): Observable <IPedidovip> {


        const service_url = ApiHelper.Instance.api_base_url + 'user';
        const ctx   =   this.getContext();


        const _headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + ctx.token
        });

        const _options = new RequestOptions( { headers: _headers } );

        const _params = this.new_pedidovip;

        return this.http.post( service_url, _params, _options)
            .map((res: Response) => res.json() )
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));

    }

}
