import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IUser } from '../../models/user.model';

@Injectable()
export class UpdateUserService extends StorageHelper {

  constructor( private http: Http ) { super(); }

  private user_id: string;
  private user_data: IUser;

  public init( user_id_: string, user_data_: IUser ): UpdateUserService {
    this.user_id = user_id_;
    this.user_data = user_data_;
    return this;
  }


  public updateUser(): Observable <IUser> {

    const service_url = ApiHelper.Instance.api_base_url + `user/${this.user_id}`;
    const ctx   =   this.getContext();

    const _headers = new Headers ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + ctx.token
    });

    const _options = new RequestOptions( { headers: _headers } );

    const _params = this.user_data;

    return this.http.put( service_url, _params, _options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));

  }

}
