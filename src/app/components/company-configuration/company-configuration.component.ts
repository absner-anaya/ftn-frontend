import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { ICompany, Company } from '../../models/company.model';
import { IUser, User } from '../../models/user.model';
import { StorageHelper } from '../../helpers/storage.helper';
import { AddCompanyService } from '../../services/company/add.company.service';
import { GetCompanyService } from '../../services/company/get.company.service';
import { UpdateCompanyService } from '../../services/company/update.company';
import { GetUserService } from '../../services/user/get.user.service';
import { UpdateUserService } from '../../services/user/update.user.service';

@Component({
  selector: 'app-company-configuration',
  templateUrl: './company-configuration.component.html',
  styleUrls: ['./company-configuration.component.scss'],
  providers: [
    AddCompanyService,
    GetCompanyService,
    UpdateCompanyService,
    GetUserService,
    UpdateUserService
  ]
})
export class CompanyConfigurationComponent extends StorageHelper implements OnInit {

  public ctx: any;
  public phoneList: Array<string> = [];
  public networkList: Array<any> = [];

  public formCompany: FormGroup;
  public _company: ICompany = new Company();
  public user_data: IUser = new User();

  constructor(
    public form: FormBuilder,
    private AddCompany: AddCompanyService,
    private GetCompany: GetCompanyService,
    private UpdateCompany: UpdateCompanyService,
    private GetUser: GetUserService,
    private UpdateUser: UpdateUserService
  ) {
    super();

    this.formCompany = form.group({
      rut: new FormControl(this._company.rut,
        Validators.compose([
          Validators.required,
        ])
      ),
      name: new FormControl(this._company.name,
        Validators.compose([
          Validators.required
        ])
      ),
      country: new FormControl(this._company.country,

      ),
      currency: new FormControl(this._company.currency,

      ),
      email: new FormControl(this._company.email,
        Validators.compose([
          Validators.required,
          Validators.pattern('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$')
        ])
      ),
      agent: new FormControl(this._company.agent,
      ),
      address: new FormControl(this._company.address,
      )
    });
  }

  ngOnInit() {
    this.ctx = this.getContext();
    //this.getUserId(this.ctx.user.id);

  }

  /**
   * getCompanyID
   */
  public getCompanyId(id: any) {
    this.GetCompany.init(id).getData().subscribe((response: any) =>  {
      console.log('get comapy id: ', response);
      // const data_: ICompany = response.content;
      this._company = response;
      console.log(this._company);
    });
  }

  /**
   * getUserId
   */
  public getUserId(id: string) {

    this.GetUser.init(id).getData().subscribe((response) => {
      this.user_data = response;
      console.log('data user: ', this.user_data);
      this.isCompany();
    });
  }

  /**
   * isCompany
   */
  public isCompany() {

    if (this.user_data.company_id != null) {
      console.log('hay compañía', this.user_data.company_id);
      this.getCompanyId(this.user_data.company_id);
      return true;
    }else {
      console.log('isCompanyNull');
      return false;
    }
  }

  /**
   * addPhone
   */
  public addPhone(item: string) {
    this.phoneList.push(item);
  }
  /**
   * delPhone
   */
  public delPhone(item: number) {
    this.phoneList.splice(item, 1);
  }

  /**
   * addNetwork
   */
  public addNetwork(item: string) {
    this.networkList.push(item);
  }

  /**
   * delNetwork
   */
  public delNetwork(item: number) {
    this.networkList.splice(item, 1);
  }

  /**
   * saveCompany
   */
  public saveCompany() {
    //if (this.isCompany()) {
    //  console.log('Hay compañía');
    //  this.updateCompany(this._company.id, this._company);
    //} else {
    //  console.log('No hay compañia');
    //  this.createCompany();
    //}
  }

  /**
   * createCompany
   */
  public createCompany() {
    this.parse();
    this.AddCompany.init(this._company).createUser().subscribe((response: any) =>  {
      console.log('response create: ', response);
      this.putUser(response.content.id);
    }, (error) => {
      console.log(error);
    });
    // tslint:disable-next-line:prefer-const
  }

  /**
   * updateCompany
   */
  public updateCompany(id: string, data: ICompany) {
    this.parse();
    this.UpdateCompany.init(id, data).update().subscribe((response: any) => {
      this._company = response;
      console.log('Update company', response);
    });
  }

  /**
   * putUser
   */
  public putUser(company_id_: string) {
    // tslint:disable-next-line:prefer-const
    this.user_data.company_id = company_id_;
    this.UpdateUser.init(this.ctx.user.id, this.user_data).updateUser().subscribe((response) =>  {
      console.log('actulización user', response);
    });
  }

  private parse() {
    this._company.rut      = this.formCompany.value.rut;
    this._company.name     = this.formCompany.value.name;
    this._company.address  = this.formCompany.value.address;
    this._company.agent    = this.formCompany.value.agent;
    this._company.email    = this.formCompany.value.email;

    console.log(this._company);
  }

}
