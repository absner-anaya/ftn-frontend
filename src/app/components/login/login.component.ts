import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { LoginAuthService } from '../../services/auth/login.auth.service';
import { SnackService } from '../../helpers/snack.helper';
import { StorageHelper } from '../../helpers/storage.helper';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginAuthService,
              SnackService,
              StorageHelper
             ]
})
export class LoginComponent implements OnInit {

  public formLogin: FormGroup;
  public email: string;
  public pass: string;
  public login: boolean;

  constructor(
        public form: FormBuilder,
        public snack: SnackService,
        private Storage: StorageHelper,
        private router: Router,
        private route: ActivatedRoute,
        private loginAuth: LoginAuthService
      ) {

        this.formLogin  = form.group({
          email: new FormControl('',
            Validators.compose([
              Validators.required,
              Validators.pattern('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$')
            ])
          ),
          password: new FormControl('',
            Validators.compose([
              Validators.required
            ])
          )
        });
      }

  ngOnInit() {
    this.login  = false;
  }

  /**
   * login
   */
  public loginApi() {
    this.router.navigate(['/cpanel/dashboard']);
    //this.login = true;
    //this.parse();
    /*this.loginAuth.init(this.email, this.pass).login().subscribe((response) =>  {
      if (response.code === 200) {
        this.Storage.saveSessionStorage('_ctx', response.data);
        this.login = false;
        this.router.navigate(['/cpanel/dashboard']);
      }
      if (response.code === 400) {
        this.login = false;
        this.snack.showSnackBar('¡Verifique sus datos de usuario!');
      }
    }, (error) => {
      this.login = false;
      this.snack.showSnackBar('¡Ah ocurrido un problema!');
    });*/
  }

  /**
   * parse
   */
  public parse() {
    this.email  = this.formLogin.value.email;
    this.pass   = this.formLogin.value.password;
  }

}
