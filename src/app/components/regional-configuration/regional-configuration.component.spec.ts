import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionalConfigurationComponent } from './regional-configuration.component';

describe('RegionalConfigurationComponent', () => {
  let component: RegionalConfigurationComponent;
  let fixture: ComponentFixture<RegionalConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionalConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionalConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
