import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertsSaleComponent } from './alerts-sale.component';

describe('AlertsSaleComponent', () => {
  let component: AlertsSaleComponent;
  let fixture: ComponentFixture<AlertsSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertsSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertsSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
