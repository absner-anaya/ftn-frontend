import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { StorageHelper } from '../../helpers/storage.helper';
import { UpdateCompanyService } from '../../services/company/update.company';
import { GetUserService } from '../../services/user/get.user.service';

import { ICompany, Company } from '../../models/company.model';
import { IUser, User } from '../../models/user.model';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss'],
  providers: [
    GetUserService,
    UpdateCompanyService
  ]
})
export class ConfigurationComponent extends StorageHelper implements OnInit {

  public closeResult: string;
  public ctx: any;
  public formModal: FormGroup;
  public data_user: IUser = new User();
  // tslint:disable-next-line:no-inferrable-types
  public pais: string = 'pais';
  public listaPais = [{
    name: 'Ecuador'
  },
  {
    name: 'Bolivia'
  }];

  constructor(
    public form: FormBuilder,
    private modalService: NgbModal,
    private UpdateCompany: UpdateCompanyService,
    private GetUser: GetUserService
  ) {
    super();

    this.formModal = form.group({
      country: new FormControl('',
        Validators.compose([
          Validators.required
        ])
      ),
      currency: new FormControl('',
        Validators.compose([
          Validators.required
        ])
      )
    });
   }

  ngOnInit() {

    this.ctx  = this.getContext();
    //this.getUserId(this.ctx.user.id);

  }

  public open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  /**
   * getUserId
   */
  public getUserId(id: string) {
    this.GetUser.init(id).getData().subscribe((response) => {
      this.data_user = response;
    }, (error) => {
      console.log(error);
    });
  }

  /**
   * updateCompany
   */
  public updateCompany() {
    const data_company: ICompany = new Company();
    data_company.country  = this.formModal.value.country;
    data_company.currency = this.formModal.value.currency;

    this.UpdateCompany.init(this.data_user.company_id, data_company).update().subscribe((response) => {
      console.log(response);
    }, (error) => {
      console.log(error);
    });


  }

}
