import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { ResetPasswordAuthService } from '../../services/auth/reset-password.auth.service';
import { SnackService } from '../../helpers/snack.helper';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  providers: [
    ResetPasswordAuthService,
    SnackService
  ]
})
export class ResetPasswordComponent implements OnInit {

  public form_reset_pass: FormGroup;
  public is_mach: boolean;

  constructor(
    public form: FormBuilder,
    private Snack: SnackService,
    private ResetPasswordAuth: ResetPasswordAuthService
  ) {

    this.form_reset_pass = form.group({
      email: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$')
        ])
      )
    });
  }

  ngOnInit() {

    this.is_mach = false;
  }

  /**
   * searchEmail
   */
  public searchEmail() {

    const email: string = this.form_reset_pass.value.email;

    this.ResetPasswordAuth.init(email).resetPass().subscribe((response) => {
      console.log(response);
      if (response.code  === 404) {
        this.Snack.showSnackBar('Email no encontrado');
      }
      if (response.code === 200) {
        this.is_mach = true;
      }
    }, (error) => {
      console.log(error);
    });
  }

}
