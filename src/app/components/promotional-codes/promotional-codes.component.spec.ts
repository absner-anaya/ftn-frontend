import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionalCodesComponent } from './promotional-codes.component';

describe('PromotionalCodesComponent', () => {
  let component: PromotionalCodesComponent;
  let fixture: ComponentFixture<PromotionalCodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionalCodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionalCodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
