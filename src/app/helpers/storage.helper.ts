/*!
 * @(#)storage.helper.ts
 * Security Helper as a Singleton, allows to crypt and secure storage data in localstorage and sessionstorage
 * @author: Diego Guevara - diegoguevara.github.io
 * @version 1.0.0
 * Created: 2016.02, Diego Guevara
 */

import { Injectable } from '@angular/core';
import { IContext, Context } from '../models/context.model';
import * as CryptoJS from 'crypto-js' ;


@Injectable()
export class StorageHelper {

    private static _instance: StorageHelper;

    constructor( ) { }

    /**
     * Singleton Instance
     *
     * @readonly
     * @static
     *
     * @memberOf StorageHelper
     */
    public static get Instance() {
        return this._instance || (this._instance = new this());
    }

    /**
     * Generate random salt string
     * @returns {string} Random string
     * @memberOf StorageHelper
     */
    generateSalt(): string {
        let text = '';
        // tslint:disable-next-line:prefer-const
        let possible = '@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!&$#%&';

        for ( let i = 0; i < 10; i++ ) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    /**
     * Save data crypted in sessionstorage
     * @param {string} key_ sessionstorage key name
     * @param {Object} value_ json object with data
     * @memberOf StorageHelper
     */
    saveSessionStorage ( key_: string, value_: Object ): void {
        if ( value_) {
            const _salt = this.generateSalt();
            const ciphertext = CryptoJS.AES.encrypt( JSON.stringify(value_) , _salt );
            window.sessionStorage.setItem( key_, _salt + ciphertext.toString() );
        }else {
            window.sessionStorage.removeItem( key_ );
        }
    }

    /**
     * Get data in sessionstorage
     *
     * @param {string} key_ sessionstorage key name
     * @returns {Object} Json Object
     * @memberOf StorageHelper
     */
    getSessionStorage ( key_: string ): Object {
        if ( window.sessionStorage[ key_ ] ) {
            const _storage_value: string = window.sessionStorage[ key_ ];
            const _salt = _storage_value.substr(0, 10 );
            const _value = _storage_value.substr(10, _storage_value.length );
            const bytes = CryptoJS.AES.decrypt( _value, _salt );
            return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        }
    }



    /**
     * Save data crypted in localstorage
     * @param {string} key_ localstorage key
     * @param {Object} value_ objeto
     * @memberOf StorageHelper
     */
    saveLocalStorage ( key_: string, value_: Object ): void {
        if ( value_) {
            const _salt = this.generateSalt();
            const ciphertext = CryptoJS.AES.encrypt( JSON.stringify(value_) , _salt );
            window.localStorage.setItem( key_, _salt + ciphertext.toString() );
        }else {
            window.localStorage.removeItem( key_ );
        }
    }

    /**
     * Get data in localstorage
     *
     * @param {string} key_ localstorage key
     * @returns {Object} Objeto
     * @memberOf StorageHelper
     */
    getLocalStorage ( key_: string ): Object {
        if ( window.localStorage[ key_ ] ) {
            const _storage_value: string = window.localStorage[ key_ ];
            const _salt = _storage_value.substr(0, 10 );
            const _value = _storage_value.substr(10, _storage_value.length );
            const bytes = CryptoJS.AES.decrypt( _value, _salt );
            return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        }
    }


    /**
     * Cast into IContext sessionstorage data in _ctx key
     * @returns {IContext} Session Context Data
     * @memberOf StorageHelper
     */
    getContext(): any {
        return this.getSessionStorage( '_ctx' ) as any;
    }

    delContext(): any {
        return window.sessionStorage.removeItem('_ctx');
    }

    protected isLogged(): boolean {

        const ctx: any = this.getContext();
        if ( ctx && ctx.token ) {
            return true;
        }
        return false;
    }

}
