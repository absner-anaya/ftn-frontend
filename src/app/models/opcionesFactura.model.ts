
export interface IOpcionesfacturas {
    id?: string;
    name?: string;
    status?: boolean;
}

export class Opcionesfacturas {
    id: string;
    name: string;
    status: boolean;
}
