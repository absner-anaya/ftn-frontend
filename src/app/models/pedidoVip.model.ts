
export interface IPedidovip {
    id?: string;
    name?: string;
    status?: boolean;
}

export class Pedidovip {
    id: string;
    name: string;
    status: boolean;
}
