import { IDepartamento } from './departamento.model';
import { IMunicipio } from './minicipio.model';
import { IPedidovip } from './pedidoVip.model';
import { IPedidoplantilla } from './pedidoPlantilla.model';
import { IPedidoestado } from './pedidoEstado.model';


export interface IPedido {
    id?: string;
    empresa?: string;
    direccion?: string;
    departamento_id?: string | IDepartamento;
    municipio_id?: string | IMunicipio;
    nit?: string;
    persona?: string;
    cargo?: string;
    telefono?: string;
    celular?: string;
    fax?: string;
    email?: string;
    vip_id?: string | IPedidovip;
    plantilla_id?: string | IPedidoplantilla;
    elaborado?: string;
    pendiente?: boolean;
    estado_id?: string | IPedidoestado;
    pedidos_seguimientos?: string;
    pedidos_recordatorio?: string;
    pedidos_alertas?: string;
    opciones_factura_id?: string;
    puntos?: number;
    pmo?: boolean;
    tracking?: string;
    avisos?: string;
    status?: boolean;

}

export class Pedido {
    id: string;
    empresa: string;
    direccion: string;
    departamento_id: string | IDepartamento;
    municipio_id: string | IMunicipio;
    nit: string;
    persona: string;
    cargo: string;
    telefono: string;
    celular: string;
    fax: string;
    email: string;
    vip_id: string | IPedidovip;
    plantilla_id: string | IPedidoplantilla;
    elaborado: string;
    pendiente: boolean;
    estado_id: string | IPedidoestado;
    pedidos_seguimientos: string;
    pedidos_recordatorio: string;
    pedidos_alertas: string;
    opciones_factura_id: string;
    puntos: number;
    pmo: boolean;
    tracking: string;
    avisos: string;
    status: boolean;
}
