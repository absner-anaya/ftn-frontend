
export interface IPedidoalerta {
    id?: string;
    titulo?: string;
    descripcion?: string;
    estado?: boolean;
    destinatario?: string;
    owner?: string;
}

export class Pedidoalerta {
    id: string;
    titulo: string;
    descripcion: string;
    estado: boolean;
    destinatario: string;
    owner: string;
}
