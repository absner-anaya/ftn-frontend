
export interface IPedidoplantilla {
    id?: string;
    name?: string;
    status?: boolean;
}

export class Pedidoplantilla {
    id: string;
    name: string;
    status: boolean;
}
