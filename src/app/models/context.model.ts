import { ICompany } from './company.model';


export interface IContext {
    id?: string;
    rut?: string;
    company_id?: string | ICompany;
    first_name?: string;
    last_name?: string;
    email?: string;
    lang?: string;
    token?: string;

}

export class Context {
    id: string;
    rut: string;
    company_id: string | ICompany;
    first_name: string;
    last_name: string;
    email: string;
    lang: string;
    token?: string;

}
