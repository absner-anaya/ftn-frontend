

export interface ICompany {
    id?: string;
    rut?: string;
    name?: string;
    logo?: string;
    country?: string;
    currency?: string;
    email?: string;
    agent?: string;
    phone?: [string];
    social_net?: [string];
    address?: string;
}

export class Company {
    id: string;
    rut: string;
    name: string;
    logo: string;
    country: string;
    currency: string;
    email: string;
    agent: string;
    phone: [string];
    social_net: [string];
    address: string;
}
