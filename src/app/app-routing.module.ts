import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PanelAdminComponent } from './components/panel-admin/panel-admin.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { OrdersComponent } from './components/orders/orders.component';
import { BillsComponent } from './components/bills/bills.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductsCategoryComponent } from './components/products-category/products-category.component';
import { ProductsCatalogsComponent } from './components/products-catalogs/products-catalogs.component';
import { AlertsComponent } from './components/alerts/alerts.component';
import { AlertsSaleComponent } from './components/alerts-sale/alerts-sale.component';
import { PromotionalCodesComponent } from './components/promotional-codes/promotional-codes.component';
import { TemplateComponent } from './components/template/template.component';
import { PointSystemComponent } from './components/point-system/point-system.component';
import { RegionalConfigurationComponent } from './components/regional-configuration/regional-configuration.component';
import { CompanyConfigurationComponent } from './components/company-configuration/company-configuration.component';
import { ConfigurationComponent } from './components/configuration/configuration.component';
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/users/users.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ProfileComponent } from './components/profile/profile.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  },
  {
    path: 'auth/login',
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: 'reset',
    component: ResetPasswordComponent,
    pathMatch: 'full'
  },
  {
    path: 'changePassword/:token/:email',
    component: ChangePasswordComponent,
    pathMatch: 'full'
  },
  {
    path: 'cpanel',
    component: PanelAdminComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'orders',
        component: OrdersComponent
      },
      {
        path: 'bills',
        component: BillsComponent
      },
      {
        path: 'clients',
        component: ClientsComponent
      },
      {
        path: 'products',
        component: ProductsComponent
      },
      {
        path: 'category',
        component: ProductsCategoryComponent
      },
      {
        path: 'catalogs',
        component: ProductsCatalogsComponent
      },
      {
        path: 'alerts',
        component: AlertsComponent
      },
      {
        path: 'saleButton',
        component: AlertsSaleComponent
      },
      {
        path: 'promotionalCodes',
        component: PromotionalCodesComponent
      },
      {
        path: 'pointSystem',
        component: PointSystemComponent
      },
      {
        path: 'config',
        component: ConfigurationComponent,
      },
      {
        path: 'config/company',
        component: CompanyConfigurationComponent
      },
      {
        path: 'config/users',
        component: UsersComponent
      },
      {
        path: 'profile',
        component: ProfileComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
