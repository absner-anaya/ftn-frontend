import { FacturacionPage } from './app.po';

describe('facturacion App', () => {
  let page: FacturacionPage;

  beforeEach(() => {
    page = new FacturacionPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
